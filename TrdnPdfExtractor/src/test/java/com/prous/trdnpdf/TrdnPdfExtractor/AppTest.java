package com.prous.trdnpdf.TrdnPdfExtractor;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.junit.Test;

import com.prous.trdnpdf.logging.MyLogger;
import com.prous.trdnpdf.logging.MyLogger.LogLevel;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void generateSdf() throws Exception {
		String[] array = new String[1];
		array[0] = "TRDN_JANSSEN";
		Execution.main(array);
	}

	@Test
	public void terminalName() {
		System.out.println(Execution.getTerminal());
	}

	@Test
	public void downloadFile() throws HttpException, IOException {

		Execution.logger = new MyLogger("mockLogger" + ".log", this.getClass(), LogLevel.Info,
				System.getProperty("user.dir"));
		HttpClient client = Execution.getAuthenticatedClient();
		Execution.downloadFile(client, "pdfTest.PDF");
	}
}
