package com.prous.trdnpdf.logging;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Date;
import java.util.Properties;

/** JDK 6 or before. */

public class MyLogger {

	
	private String filePath = null;
    

	public enum LogLevel {
		NotImportant(1, "NOT IMPORTANT"), Info(2, "INFO"), Warning(3, "WARNING"), Error(
				4, "ERROR");

		private final int value;
		private final String name;

		private LogLevel(int value, String name) {
			this.value = value;
			this.name = name;
		}

		public int getValue() {
			return value;
		}

		public String getName() {
			return name;
		}

	}

	private File file;
	private Writer output;
	private Class clazz;
	private LogLevel level;
	private Boolean onlyLevel = false;
	private Boolean logErrors = false;
	private File errorFile;
	private Writer errorWriter;

	public MyLogger(String filename, Class clazz, LogLevel lev, String logPath) throws IOException {
		level = lev;
		this.clazz = clazz;
		filePath = logPath;
		File dir = new File(filePath);
		file = new File(filePath + filename);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		if (!file.exists()) {
			file.createNewFile();
		}
		output = new BufferedWriter(new FileWriter(file,true));
		onlyLevel =false;
		logErrors = false;
	}

	public MyLogger(String filename, Class clazz, LogLevel lev, Boolean hasErr, boolean b, String logPath)
			throws IOException {
		this(filename, clazz, lev, b, logPath);

		if (hasErr) {
			logErrors = hasErr;
			errorFile = new File(filePath + "Error " + filename);
			errorWriter = new BufferedWriter(new FileWriter(errorFile,true));
		}

	}

	public MyLogger(String filename, Class clazz, LogLevel lev, boolean b, String logPath) throws IOException {
		this(filename, clazz, lev, logPath);
		onlyLevel = b;
	}

	public void close() throws IOException {
		output.close();
	}

	/**
	 * Fetch the entire contents of a text file, and return it in a String. This
	 * style of implementation does not throw Exceptions to the caller.
	 * 
	 * @param aFile
	 *            is a file which already exists and can be read.
	 */
	private String write() {
		// ...checks on aFile are elided
		StringBuilder contents = new StringBuilder();

		try {
			// use buffering, reading one line at a time
			// FileReader always assumes default encoding is OK!
			BufferedReader input = new BufferedReader(new FileReader(file));
			try {
				String line = null; // not declared within while loop
				/*
				 * readLine is a bit quirky : it returns the content of a line
				 * MINUS the newline. it returns null only for the END of the
				 * stream. it returns an empty String if two newlines appear in
				 * a row.
				 */
				while ((line = input.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return contents.toString();
	}

	/**
	 * Change the contents of text file in its entirety, overwriting any
	 * existing text.
	 * 
	 * This style of implementation throws all exceptions to the caller.
	 * 
	 * @param aFile
	 *            is an existing file which can be written to.
	 * @throws IllegalArgumentException
	 *             if param does not comply.
	 * @throws FileNotFoundException
	 *             if the file does not exist.
	 * @throws IOException
	 *             if problem encountered during write.
	 */
	private void write(String aContents) throws FileNotFoundException, IOException {

		// use buffering

		try {
			// FileWriter always assumes default encoding is OK!
			output.append(aContents);
			output.append(System.getProperty("line.separator"));
		} finally {
			output.flush();
		}
	}

	private void writeError(String aContents) throws FileNotFoundException, IOException {

		// use buffering

		try {
			// FileWriter always assumes default encoding is OK!
			errorWriter.append(aContents);
			errorWriter.append(System.getProperty("line.separator"));
		} finally {
			errorWriter.flush();
		}
	}

	public void error(String text) throws FileNotFoundException, IOException {
		log(LogLevel.Error, text);
	}

	public void warn(String text) throws FileNotFoundException, IOException {
		log(LogLevel.Warning, text);
		if (logErrors) {
			writeError(text);
		}
	}

	public void info(String text) throws FileNotFoundException, IOException {
		log(LogLevel.Info, text);
	}

	public void bullshit(String text) throws FileNotFoundException, IOException {
		log(LogLevel.NotImportant, text);
	}

	private void log(LogLevel lv, String text) throws FileNotFoundException, IOException {
		String out = "[" + lv.name + "] (" + clazz.getName() + ") " + new Date().toString() + " " + text;
		if (onlyLevel) {
			if (level.equals(lv)) {
				try {
					write(out);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			if (level.value <= lv.value) {
				write(out);
			}
		}

		if (logErrors && lv.equals(LogLevel.Error)) {
			writeError(out);
		}
	}
}