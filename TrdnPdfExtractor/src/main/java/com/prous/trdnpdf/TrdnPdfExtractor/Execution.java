package com.prous.trdnpdf.TrdnPdfExtractor;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.management.ServiceNotFoundException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang3.StringUtils;

import com.prous.trdnpdf.logging.MyLogger;
import com.prous.trdnpdf.logging.MyLogger.LogLevel;

public class Execution {
	public static Connection con;
	public static ResultSet rs;
	public static MyLogger logger;
	public static final Integer MIN_FILESIZE = 17500;
	public static final String queryEmail = "SELECT C.email_from_name, C.email_from_address, C.email_to, C.email_cc, C.email_bcc, C.email_subject, C.email_body, C.email_active, C.email_customer_from_name, C.email_customer_from_address, C.email_customer_to, C.email_customer_cc, C.email_customer_bcc, C.email_customer_subject, C.email_customer_body, C.email_customer_active, E.customer, F.EMAIL_HOST, F.EMAIL_USER, F.EMAIL_PASSWORD FROM EXT_TCUSTOMERS C, EXT_TTYPE3 E, EXT_TCONFIGURATION F WHERE C.NAME = E.CUSTOMER AND E.status = 'PENDING' AND E.next_extraction <= SYSDATE AND E.terminal = :pTerminal AND E.CUSTOMER = :pExtractionName AND NOT EXISTS (SELECT 1 FROM EXT_TTYPE3 ET3 WHERE ET3.STATUS = 'BEGIN' AND ET3.terminal = :pTerminal AND ET3.customer <> E.CUSTOMER)";
	// public static final String queryEmail =
	// "SELECT C.email_from_name, C.email_from_address, C.email_to, C.email_cc, C.email_bcc, C.email_subject, C.email_body, C.email_active, C.email_customer_from_name, C.email_customer_from_address, C.email_customer_to, C.email_customer_cc, C.email_customer_bcc, C.email_customer_subject, C.email_customer_body, C.email_customer_active, E.customer, F.EMAIL_HOST, F.EMAIL_USER, F.EMAIL_PASSWORD FROM EXT_TCUSTOMERS C, EXT_TTYPE3 E, EXT_TCONFIGURATION F WHERE C.NAME = E.CUSTOMER AND E.CUSTOMER = 'TRDN_JANSSEN' AND NOT EXISTS (SELECT 1 FROM EXT_TTYPE3 ET3 WHERE ET3.STATUS = 'BEGIN' AND ET3.terminal = 'EXTRVM0' AND ET3.customer <> E.CUSTOMER) AND ROWNUM<=1";
	// // for
	// testing
	public static String filepath;
	public static String filename;
	public static String dateOfExtraction;

	public static void main(String[] args) throws SQLException, IOException {

		String exName = args[0] == null ? "" : args[0];
		try {
			dateOfExtraction = args[1];
		} catch (Exception e) {
			dateOfExtraction = null;
		}

		filepath = getPath() + System.getProperty("file.separator") + exName + System.getProperty("file.separator")
				+ getYesterdayDateString();
		filename = "THOMSONREUTERSDRUGNEWSTODAY" + getYesterdayDateString();
		Boolean shouldRun = false;
		String comments = null;
		con = getDBConnection();
		Boolean valid = false;
		logger = new MyLogger(filename + ".log", Execution.class, LogLevel.Info, filepath
				+ System.getProperty("file.separator"));
		Date start = new Date();
		Map<String, String> email = null;
		File file = null;
		try {

			email = getEmailInfo(exName);
			shouldRun = !email.isEmpty();
			if (shouldRun) {

				startProcess(exName);

				createDirectory(filepath, exName);

				int count = 0;
				HttpClient client = null;
				try {
					client = getAuthenticatedClient();
				} catch (Exception e) {
					throw new ServiceNotFoundException("Client couldn't reach server");
				}

				while (!valid && count < 3) {
					count++;
					Thread.sleep(1000 * 10);
					logger.info("Attempt #" + count + " starting");
					try {
						file = downloadFile(client, filepath + System.getProperty("file.separator") + filename + ".pdf");
					} catch (Exception e) {
						System.out.println(e.toString());
					}
					valid = validFile(file);
					String logms = "Attempt #" + count + (valid ? " succeed" : " failed");

					if (valid) {
						logger.info(logms);
					} else {
						logger.warn(logms);
					}

				}
				if (!valid) {
					throw new FileNotFoundException("File empty or not found!");
				}

			}
		} catch (Exception e) {
			logger.error(e.toString());
			comments = e.toString();
			System.err.println(comments);
			System.err.println(e.getMessage());
		}

		if (shouldRun) {

			Boolean activeEmail = email.get("email_active").equals("1");
			Boolean activeCustomerEmail = email.get("email_customer_active").equals("1");
			String subject = exName + (valid ? " SUCCESFUL " : " ERROR ") + getYesterdayDateString();
			if (valid) {
				if (activeEmail) {
					logger.info("Sending internal email");
					sendEmail(email.get("email_to"), email.get("email_from_address"), email.get("email_from_name"),
							email.get("email_cc"), email.get("email_bcc"), email.get("EMAIL_HOST"), subject,
							"Another battle we have won!", email.get("EMAIL_USER"), email.get("EMAIL_PASSWORD"),
							file.getAbsolutePath());
					logger.info("Internal email sent!");
				}

				if (activeCustomerEmail) {
					logger.info("Sending customer email");
					sendEmail(email.get("email_customer_to"), email.get("email_customer_from_address"),
							email.get("email_customer_from_name"), email.get("email_customer_cc"),
							email.get("email_customer_bcc"), email.get("EMAIL_HOST"),
							"Thomson Reuters Drug News - Today's News: " + getYesterdayDateString(),
							email.get("email_customer_body"), email.get("EMAIL_USER"), email.get("EMAIL_PASSWORD"),
							file.getAbsolutePath());
					logger.info("Internal email sent!");
				}
			} else {
				if (activeEmail) {
					logger.info("Sending internal email");
					sendEmail(email.get("email_to"), email.get("email_from_address"), email.get("email_from_name"),
							email.get("email_cc"), email.get("email_bcc"), email.get("EMAIL_HOST"), subject, comments,
							email.get("EMAIL_USER"), email.get("EMAIL_PASSWORD"), null);
					logger.info("Internal email sent!");
				}

			}

			Date end = new Date();
			try {
				logger.info("Finishing process");
				finishProcess(exName, start, end, comments);
				logger.info("Process finished");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.toString());
			}
		} else {
			logger.info("Nothing to do, exiting.");
		}
		con.close();

	}

	public static boolean validFile(File file) throws FileNotFoundException, IOException {
		logger.bullshit("File size" + file.length());
		logger.bullshit("File " + (file != null ? "is not" : "is") + " null");
		return file != null && file.length() > MIN_FILESIZE;

	}

	public static Connection getDBConnection() throws SQLException, IOException {
		Properties prop = new Properties();

		prop.load(Execution.class.getResourceAsStream("config.properties"));

		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

		Connection conn = null;
		String jdbcUrl = "jdbc:oracle:thin:@" + prop.getProperty("host") + ":" + prop.getProperty("port") + ":"
				+ prop.getProperty("sid");

		String user = prop.getProperty("user");
		String password = prop.getProperty("password");

		conn = DriverManager.getConnection(jdbcUrl, user, password);

		conn.setAutoCommit(false);

		return conn;
	}

	public static void startProcess(String extractName) throws SQLException, FileNotFoundException, IOException {
		logger.info("Starting process");
		CallableStatement cs = null;
		cs = con.prepareCall("{call PRODUCTIZATION_PKG.EXT_START_JOB_TYPE3(?)}");
		cs.setString(1, extractName);
		cs.executeQuery();
		logger.info("Process started");

	}

	public static void finishProcess(String extractName, Date start, Date end, String comments) throws SQLException {

		CallableStatement cs = null;
		cs = con.prepareCall("{call PRODUCTIZATION_PKG.EXT_PUPDATE_JOB_TYPE3(?,?,?,?,?,?,?,?)}");

		/*
		 * 
		 * Stored Procedure PRODUCTIZATION_PKG.EXT_PUPDATE_JOB_TYPE3 Parameters
		 * aCUSTOMER: [EXTRACTION_NAME]. aSTATUS: "FINISHED" or "ERROR"
		 * depending the outcome aDELIVERY: 0 aINI_EXTR: Extraction starting
		 * date aFIN_EXTR: Extraction ending date aCOMMENTS: "OK" if successful
		 * or Exception trace in case of fail , si fue exitoso aFILE_SIZE: 0
		 * aPRODUCTS_COUNT: 0
		 */

		Boolean errors = comments != null;
		cs.setString(1, extractName);
		cs.setString(2, errors ? "ERROR" : "FINISHED");
		cs.setInt(3, 0);
		cs.setDate(4, new java.sql.Date(start.getTime()));
		cs.setDate(5, new java.sql.Date(end.getTime()));
		cs.setString(6, comments);
		cs.setInt(7, 0);
		cs.setInt(8, 0);

		cs.executeQuery();
	}

	public static File downloadFile(HttpClient client, String fileN) throws FileNotFoundException, IOException {
		PostMethod download = new PostMethod("http://drugnews.thomson-pharma.com/ddn/dataset.do") {
			@Override
			public boolean getFollowRedirects() {
				return true;
			}
		};
		;
//		download.setDoAuthentication(true);
//		download.addParameter(new NameValuePair("j_username", "montse"));
//		download.addParameter(new NameValuePair("j_password", "marc0505"));
		download.addParameter(new NameValuePair("type", "TODAY"));
		download.addParameter(new NameValuePair("dateofreport", getYesterdayDateString()));
		download.addParameter(new NameValuePair("format", "pdf"));
		download.addParameter(new NameValuePair("title", "NEW"));

		logger.bullshit("BEFORE EXECUTING");
		logger.bullshit(download.getProxyAuthState().toString());
		logger.bullshit("Realm: " + download.getProxyAuthState().getRealm());

		File file = null;
		logger.info("Calling " + download.getURI());
		logger.info("Parameters " + download.getQueryString());
		int status;
		try {
			logger.bullshit("Executing download URL");
			status = client.executeMethod(download);
			logger.bullshit("Downloading finished, proceeding to check HTTP status");
			if (status == HttpStatus.SC_OK) {
				logger.bullshit("HTTP status correct");
				file = new File(fileN);
				InputStream in = new BufferedInputStream(download.getResponseBodyAsStream());

				FileOutputStream fos = new FileOutputStream(file);

				logger.bullshit("Reading file...");
				int length = -1;
				byte[] buffer = new byte[1024];// buffer for portion of data
				// from
				// connection
				while ((length = in.read(buffer)) > -1) {
					fos.write(buffer, 0, length);
				}
				fos.close();
				in.close();
			} else {
				logger.bullshit("HTTP status incorrect");
				logger.bullshit(download.getStatusText());
				logger.bullshit("Status code" + status);

			}

			logger.bullshit("AFTER EXECUTING");
			logger.bullshit(download.getProxyAuthState().toString());
			logger.bullshit("Realm: " + download.getProxyAuthState().getRealm());

		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;

	}

	public static HttpClient getAuthenticatedClient() throws HttpException, IOException {
		HttpClient client = new HttpClient();

		client.getParams().setParameter(HttpMethodParams.USER_AGENT,
				"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

		client.getState().setCredentials(
				new AuthScope("http://drugnews.thomson-pharma.com/ddn/j_spring_security_check.do", 443, AuthScope.ANY_REALM),
				new UsernamePasswordCredentials("montse", "marc0505"));

		PostMethod get = new PostMethod("http://drugnews.thomson-pharma.com/ddn/j_spring_security_check.do") {
			@Override
			public boolean getFollowRedirects() {
				return true;
			}

		};

		get.setDoAuthentication(true);
		get.addParameter(new NameValuePair("j_username", "montse"));
		get.addParameter(new NameValuePair("j_password", "marc0505"));
		logger.info("Calling " + get.getQueryString());
		logger.info("Calling " + get.getURI());
		try {
			client.executeMethod(get);
			
			logger.bullshit(get.getResponseBodyAsString());
			logger.bullshit(get.getStatusText());
			
		} catch (Exception e) {
			logger.bullshit("Client couldn't be created");
		} finally {
			// release any connection resources used by the method
			get.releaseConnection();
		}
		return client;
	}

	public static void sendEmail(String to, String from, String fromName, String cc, String bcc, String host,
			String subject, String messageContent, String user, String password, String filepath)
			throws UnsupportedEncodingException {

		// Get the Session object.
		Session session = getSession(host, user, password);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from, fromName));

			// Set To: header field of the header.

			String[] tos = to.split(";");
			for (int i = 0; i < tos.length; i++) {

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(tos[i]));
			}

			// Set CC and BCC
			if (StringUtils.isNotEmpty(cc)) {
				String[] ccs = cc.split(";");
				for (int i = 0; i < ccs.length; i++) {
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccs[i]));
				}

			}

			if (StringUtils.isNotEmpty(bcc)) {
				String[] bccs = bcc.split(";");
				for (int i = 0; i < bccs.length; i++) {
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccs[i]));
				}

			}

			// Set Subject: header field
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText((StringUtils.isNotEmpty(messageContent) ? messageContent : ""));

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			if (StringUtils.isNotEmpty(filepath)) {
				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(filepath);
				messageBodyPart.setDataHandler(new DataHandler(source));
				String[] filepathArray = filepath.split("\\\\");
				String filename = filepathArray[filepathArray.length - 1];
				messageBodyPart.setFileName(filename);
				multipart.addBodyPart(messageBodyPart);
			}
			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public static String getPath() {

		return System.getProperty("user.dir");
	}

	public static void createDirectory(String path, String extractionName) throws FileNotFoundException, IOException {
		logger.info("Creating directory");
		File createdir = new File(filepath);
		if (!createdir.exists()) {
			createdir.mkdirs();
		}
		logger.info("Directory created");

	}

	public static String getYesterdayDateString() {
		if (dateOfExtraction != null) {
			return dateOfExtraction;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	public static Map<String, String> getEmailInfo(String exName) throws SQLException, FileNotFoundException,
			IOException {
		logger.info("Checking if there is a pending process");
		Statement stmt = con.createStatement();
		String query = queryEmail.replace(":pExtractionName", "'" + exName + "'");
		query = query.replace(":pTerminal", "'" + getTerminal() + "'");
		Map<String, String> emailvalues = new HashMap<String, String>();
		rs = stmt.executeQuery(query);
		while (rs.next()) {
			String field, value;
			field = "email_active";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_from_name";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_from_address";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_to";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_cc";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_bcc";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_active";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_from_name";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_from_address";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_to";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_cc";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_bcc";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "email_customer_body";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "EMAIL_USER";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "EMAIL_PASSWORD";
			value = rs.getString(field);
			emailvalues.put(field, value);

			field = "EMAIL_HOST";
			value = rs.getString(field);
			emailvalues.put(field, value);
		}
		rs.close();
		return emailvalues;
	}

	public static String getTerminal() {
		String hostname = "Unknown";

		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			System.out.println("Hostname can not be resolved");
		}
		return hostname;
	}

	public static Session getSession(String smtp, String user, String pass) {
		Authenticator authenticator = new Authenticator(user, pass);

		Properties properties = new Properties();
		if (StringUtils.isNotEmpty(pass)) {
			properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
			properties.setProperty("mail.smtp.auth", "true");

		}

		properties.setProperty("mail.smtp.host", smtp);
		properties.setProperty("mail.smtp.port", "25");

		return Session.getInstance(properties, authenticator);
	}

}
